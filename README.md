# arduSerial2PPM

## PPM control from serial interface

Arduino sketch to control PPM devices, like RC autopilots, from serial communication
- Using HW interrupts for precise timming

## Tested receivers
- Afroflight Naze32

---
Modified from:
generate-ppm-signal(V0.2): https://code.google.com/archive/p/generate-ppm-signal/downloads
