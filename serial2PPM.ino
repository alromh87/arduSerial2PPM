//this programm will put out a PPM signal

//#define DEBUG

//////////////////////CONFIGURATION///////////////////////////////
#define channel_number 8  //set the number of chanels
#define default_servo_value 1500  //set the default servo value
#define PPM_FrLen 22500  //set the PPM frame length in microseconds (1ms = 1000µs)
#define PPM_PulseLen 300  //set the pulse length
#define onState 1  //set polarity of the pulses: 1 is positive, 0 is negative
#define sigPin 10  //set PPM signal output pin on the arduino
//////////////////////////////////////////////////////////////////


/*this array holds the servo values for the ppm signal
 change theese values in your code (usually servo values move between 1000 and 2000)*/
int ppm[channel_number];
int index = 0;
byte serialIn;
bool almostDone = false;

short data[channel_number];

//union{
//  byte asByte[2],
//  short asShort[1]
//} value
short value;

void setup(){  
  //initiallize default ppm values
  for(int i=0; i<channel_number; i++){
    ppm[i]= default_servo_value;
  }

  pinMode(sigPin, OUTPUT);
  digitalWrite(sigPin, !onState);  //set the PPM signal pin to the default state (off)
  
  cli();
  TCCR1A = 0; // set entire TCCR1 register to 0
  TCCR1B = 0;
  
  OCR1A = 100;  // compare match register, change this
  TCCR1B |= (1 << WGM12);  // turn on CTC mode
  TCCR1B |= (1 << CS11);  // 8 prescaler: 0,5 microseconds at 16mhz
  TIMSK1 |= (1 << OCIE1A); // enable timer compare interrupt
  sei();
  Serial.begin(9600);
  Serial.println("serial2PPM");
}

void loop(){
  if(Serial.available()){
    serialIn = Serial.read();
#ifdef DEBUG
//    Serial.print(index);    Serial.print("**: ");    Serial.println(serialIn);
#endif

    if(almostDone && serialIn == 127){ //Encontramos marcador actualizar canales obtenidos
//      Serial.print(index/2);
      Serial.print(data[7]);
      for(int i = 0;i<index/2;i++){
        ppm[i] = (int)data[i];
#ifdef DEBUG
        Serial.print(i);        Serial.print(": ");        Serial.println(data[i]);
#endif
      }
      index = 0;
      almostDone = false;
      return;
    }
    almostDone = (serialIn == 127);
    
    if(index >= (channel_number*2)+1){//No hemos encontrado marcadores: //TODO: recorrer
        index = 0;
        almostDone = false;
        Serial.println("Overflow");
        return;
    }
    index++;
    if(index%2 != 0){
      value = 0x0000 | serialIn;
    }else{
      value = (value<<8) | serialIn;
      data[(index/2)-1] = value;
    }
  }
}

ISR(TIMER1_COMPA_vect){  //leave this alone
  static boolean state = true;
  
  TCNT1 = 0;
  
  if(state) {  //start pulse
    digitalWrite(sigPin, onState);
    OCR1A = PPM_PulseLen * 2;
    state = false;
  }
  else{  //end pulse and calculate when to start the next pulse
    static byte cur_chan_numb;
    static unsigned int calc_rest;
  
    digitalWrite(sigPin, !onState);
    state = true;

    if(cur_chan_numb >= channel_number){
      cur_chan_numb = 0;
      calc_rest = calc_rest + PPM_PulseLen;// 
      OCR1A = (PPM_FrLen - calc_rest) * 2;
      calc_rest = 0;
    }
    else{
      OCR1A = (ppm[cur_chan_numb] - PPM_PulseLen) * 2;
      calc_rest = calc_rest + ppm[cur_chan_numb];
      cur_chan_numb++;
    }     
  }
}
